/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.metier;

import com.gsb.mapharma.dao.PharmacienDAO;
import com.gsb.mapharma.vo.ProfilVO;

/**
 *
 * @author addev
 */
public class Pharmacien {
    
    private ProfilVO pharmacien;
    private PharmacienDAO dao;
    
    public Pharmacien() {
        dao = new PharmacienDAO();
    }
    
    public ProfilVO getConnection(String login, String mdp) {
        
        //return dao.isPharmacienByLogin(login, mdp);
        return dao.getPharmacienByLogin(login, mdp);
    }
    
    public ProfilVO createUser(String matricule,String nom,String prenom,String password) {
        return dao.creationProfil(matricule,nom,prenom,password);
    }
    
}
