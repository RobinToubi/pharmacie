/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.metier;

import com.gsb.mapharma.dao.MedicamentDAO;
import com.gsb.mapharma.vo.MedicamentVO;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author addev
 */
public class Medicament {

  private final ObservableList<MedicamentVO> medicaments;
  
  private MedicamentDAO dao;
  
  
 public Medicament () {
     medicaments = FXCollections.observableArrayList();
 }
  
 public ObservableList<MedicamentVO> getMedicaments(){
     this.dao = new MedicamentDAO();
     medicaments.removeAll(medicaments);
     medicaments.addAll(dao.readMedicamentByArrayList());
     return medicaments;
     
 }
 
    
}
