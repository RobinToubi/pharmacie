/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.vo;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author addev
 */
public class ProfilVO {
    
    private StringProperty TxtMatri;
    private StringProperty TxtNom;
    private StringProperty TxtPrenom;
    private StringProperty TxtRole;
    private StringProperty TxtMDP;
    private StringProperty TxtNewMDP;
    private String textPassword;
    
    public ProfilVO (){
        super();
        this.TxtMatri = new SimpleStringProperty();
        this.TxtMDP = new SimpleStringProperty();
        this.TxtNom = new SimpleStringProperty();
        this.TxtPrenom = new SimpleStringProperty();
        this.TxtRole = new SimpleStringProperty();
        this.textPassword = new String();
    }

    /**
     * @return the TxtMatri
     */
    public String getTxtMatri() {
        return this.TxtMatri.get();
    }

    /**
     * @param TxtMatri the TxtMatri to set
     */
    public void setTxtMatri(String TxtMatri) {
        this.TxtMatri.set(TxtMatri);
    }

    /**
     * @return the TxtNom
     */
    public String getTxtNom() {
        return this.TxtNom.get();
    }

    /**
     * @param TxtNom the TxtNom to set
     */
    public void setTxtNom(String TxtNom) {
        this.TxtNom.set(TxtNom);
    }

    /**
     * @return the TxtPrenom
     */
    public String getTxtPrenom() {
        return this.TxtPrenom.get();
    }

    /**
     * @param TxtPrenom the TxtPrenom to set
     */
    public void setTxtPrenom(String TxtPrenom) {
        this.TxtPrenom.set(TxtPrenom);
    }

    /**
     * @return the TxtRole
     */
    public String getTxtRole() {
        return this.TxtRole.get();
    }

    /**
     * @param TxtRole the TxtRole to set
     */
    public void setTxtRole(String TxtRole) {
        this.TxtRole.set(TxtRole);
    }

    /**
     * @return the TxtMDP
     */
    public StringProperty getTxtMDP() {
        return TxtMDP;
    }

    /**
     * @param TxtMDP the TxtMDP to set
     */
    public void setTxtMDP(StringProperty TxtMDP) {
        this.TxtMDP = TxtMDP;
    }

    /**
     * @return the TxtNewMDP
     */
    public StringProperty getTxtNewMDP() {
        return TxtNewMDP;
    }

    /**
     * @param TxtNewMDP the TxtNewMDP to set
     */
    public void setTxtNewMDP(StringProperty TxtNewMDP) {
        this.TxtNewMDP = TxtNewMDP;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return textPassword;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.textPassword = password;
    }
    
}
