/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.vo;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.adapter.JavaBeanStringProperty;

/**
 *
 * @author addev
 */
public class MedicamentVO {
    
    private StringProperty colNomMed;
    private StringProperty colFormeMed;
    private StringProperty colCodeCIS;
    private StringProperty codeCIS;
    
    public MedicamentVO (){
        super();
        this.colNomMed = new SimpleStringProperty();
        this.colFormeMed= new SimpleStringProperty();
        this.colCodeCIS = new SimpleStringProperty();
        this.codeCIS = new SimpleStringProperty();
    }

    /**
     * @return the colNomMed
     */
    public String getColNomMed() {
        return colNomMed.get();
    }

    /**
     * @param colNomMed the colNomMed to set
     */
    public void setColNomMed(String colNomMed) {
        this.colNomMed.set(colNomMed);
    }

    /**
     * @return the colFormeMed
     */
    public String getColFormeMed() {
        return colFormeMed.get();
    }

    /**
     * @param colFormeMed the colFormeMed to set
     */
    public void setColFormeMed(String colFormeMed) {
        this.colFormeMed.set(colFormeMed);
    }

    /**
     * @return the colCodeCIS
     */
    public String getColCodeCIS() {
        return colCodeCIS.get();
    }

    /**
     * @param colCodeCIS the colCodeCIS to set
     */
    public void setColCodeCIS(String colCodeCIS) {
        this.colCodeCIS.set(colCodeCIS);
    }

    /**
     * @return the codeCIS
     */
    public String getCodeCIS() {
        return codeCIS.get();
    }

    /**
     * @param codeCIS the codeCIS to set
     */
    public void setCodeCIS(String codeCIS) {
        this.codeCIS.set(codeCIS);
    }
    
    
    
}
