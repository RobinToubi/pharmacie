/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.metier.Pharmacien;
import com.gsb.mapharma.vo.ProfilVO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author addev
 */
public class LoginController implements Initializable {

    @FXML private TextField matricule;
    @FXML private PasswordField password;
    @FXML private Button btnReinitialiser;
    @FXML private Button btnConnexion;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        btnReinitialiser.setOnAction((ActionEvent event) -> {
            reinitialiser();
        });
        btnConnexion.setOnAction((ActionEvent co) -> {
            try {
                connexion();
            } catch (IOException ex) {
                Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
    }

    public void reinitialiser() {
        matricule.setText("");
        password.setText("");
    }

    public void connexion() throws IOException {
        Pharmacien metier = new Pharmacien();
        ProfilVO monProfil = metier.getConnection(matricule.getText(), password.getText());
        if (monProfil != null) {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("Menu.fxml"));
            Parent menu = loader.load();
            SingletonProfil sProfil = SingletonProfil.getInstance();
            sProfil.setProfil(monProfil);
            SingletonStage st = SingletonStage.getInstance();
            Stage stageLogin = st.getStage();
            Scene sceneMenu = new Scene(menu);
            stageLogin.setScene(sceneMenu);
            stageLogin.show();
            
        }

    }

}
