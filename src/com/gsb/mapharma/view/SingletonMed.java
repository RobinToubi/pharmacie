/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.vo.MedicamentVO;

/**
 *
 * @author rgimenez
 */
public class SingletonMed {
    private MedicamentVO med;
    private static SingletonMed INSTANCE;
    
    private SingletonMed(){
        med = new MedicamentVO();
    }
    
    public static SingletonMed getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new SingletonMed();
        }
        return INSTANCE;
    }
    
    public MedicamentVO getMed(){
        return med;
    }
    
    public void setMed(MedicamentVO med){
        this.med = med;
    }
}
