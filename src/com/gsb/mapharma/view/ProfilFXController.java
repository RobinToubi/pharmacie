/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.metier.Pharmacien;
import com.gsb.mapharma.vo.ProfilVO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author addev
 */
public class ProfilFXController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private TextField txtMatricule;
    @FXML
    private TextField txtNom;
    @FXML
    private TextField txtPrenom;
    @FXML
    private TextField txtRole;

    @FXML
    private PasswordField passwordOld;
    @FXML
    private PasswordField passwordNew;
    @FXML
    private PasswordField passwordGo;

    @FXML
    private Button btnReinitialiser;
    @FXML
    private Button btnValider;
    @FXML
    private Button btnMenu;

    private ProfilVO monProfil;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        monProfil = SingletonProfil.getInstance().getProfil();
        txtMatricule.setText(this.monProfil.getTxtMatri());
        txtNom.setText(this.monProfil.getTxtNom());
        txtPrenom.setText(this.monProfil.getTxtPrenom());
        txtRole.setText(this.monProfil.getTxtRole());
    }

    public void reinitialisebutton() {
        passwordOld.setText("");
        passwordNew.setText("");
        passwordGo.setText("");
    }

        /**
         * public void validerbutton(ActionEvent event) { Button btnValider =
         * new Button("Valider"); btnValider.setOnAction((ActionEvent c);          *
         *
         * });
         */
    
    


//    /**
//     * @return the idPharma
//     */
//    public ProfilVO getMonProfil() {
//        return monProfil;
//    }
//
//    /**
//     * @param idPharma the idPharma to set
//     */
//    public void setMonProfil(ProfilVO idPharma) {
//        this.monProfil = idPharma;
//        
//    }

    public void menuSwitch(){
        SceneMenu test = new SceneMenu("Menu.fxml");//, getMonProfil());
    }
    
}
