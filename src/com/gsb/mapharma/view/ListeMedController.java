/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.metier.Medicament;
import com.gsb.mapharma.vo.MedicamentVO;
import com.sun.prism.impl.Disposer.Record;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.util.Callback;

/**
 * FXML Controller class
 *
 * @author addev
 */
public class ListeMedController implements Initializable {

    @FXML private Button btnMenu;
    @FXML private TableView tableViewListeMed;
    @FXML private TableColumn colNomMed;
    @FXML private TableColumn colFormeMed;
    @FXML private TableColumn colCodeCIS;
    @FXML private Button btnSearch;
    @FXML private TextField textFieldSearch;
    
    private Medicament metier;
    
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        metier = new Medicament();
        colNomMed.setCellValueFactory(new PropertyValueFactory("colNomMed"));
        colFormeMed.setCellValueFactory(new PropertyValueFactory("colFormeMed"));
        colCodeCIS.setCellValueFactory(new PropertyValueFactory("colCodeCIS"));
        tableViewListeMed.getItems().clear();
        tableViewListeMed.getItems().addAll(metier.getMedicaments());
        
        Callback<TableColumn, TableCell> stringCellFactory = new Callback<TableColumn, TableCell>() {
            @Override
            public TableCell call(TableColumn p) {
                MyStringTableCell cell = new MyStringTableCell();
                cell.addEventFilter(MouseEvent.MOUSE_CLICKED, new MyEventHandler());
                return cell;
            }
        };
        
        colNomMed.setCellFactory(stringCellFactory);
        colFormeMed.setCellFactory(stringCellFactory);
        colCodeCIS.setCellFactory(stringCellFactory);
        
    } 
    public void menuSwitch(){
        SceneMenu test = new SceneMenu("Menu.fxml");
    }
    
    public void panierSwitch() {
        SceneMenu test = new SceneMenu("Panier.fxml");
    }
    
    class MyStringTableCell extends TableCell<Record, String> {

        @Override
        public void updateItem(String item, boolean empty) {
            super.updateItem(item, empty);
            setText(empty ? null : getString());
            setGraphic(null);
        }

        private String getString() {
            return getItem() == null ? "" : getItem().toString();
        }
    }
    
    class MyEventHandler implements EventHandler<MouseEvent> {

         
        @Override
        public void handle(MouseEvent t) {
            MedicamentVO recup = new MedicamentVO();
            SingletonMed.getInstance().setMed(recup);
            TableCell c = (TableCell) t.getSource();
            String bonjour = c.getText(); // R?cup?rer le nom du m?dicament
            Integer index = c.getIndex(); // R?cup?rer le num?ro de la ligne
            recup.setCodeCIS(bonjour/*.toString())*/);
//            TableColumn column = colCodeCIS;
//            Object data = column.getCellObservableValue(index).getValue();

            /* Peut-etre penser a cette solution : 
                Faire en sorte que lorsqu'on clique sur une ligne on recupere le code CIS,
                puis dans la classe InfoMedController faire une requete SQL permettant de recuperer
                les donnees du medicament (nom,voie d'administration) puis les mettre dans les TextField.
                Seul soucis a regler, probleme de connexion a la base de donnees.
                Puis pourquoi pas mettre une image par medicament (ex : codeCIS.png !*/ 

            //System.out.println(data.toString());
            System.out.println(index);
            System.out.println(bonjour);
            SceneMenu oui = new SceneMenu("InfoMed.fxml");
        }
        
        
    }
    
}



//final Button btnSearch; = new Button("Search");
    //searchButton.setOnAction(new EventHandler<ActionEvent>() {
        //@Override
        //public void handle(ActionEvent e) {
            //Person person = new Person(addFirstName.getText(), addLastName.getText(), addAddress.getText(), addPhoneNo.getText(),
                    //addDateOfBirth.getText(), addEmail.getText(), addGender.getText());
            //addFirstName.clear();
            //addLastName.clear();
            //addAddress.clear();
            
       // }
   // });