package com.gsb.mapharma.view;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;


public class CommandesController implements Initializable {
    
    @FXML
    private Button butMenu;
    
    @FXML
    private Button butRechercher;
    
    @FXML
    private TextField textFieldRechercher;
    
    @FXML
    private TableColumn colCommande;
    
    @FXML
    private TableColumn colMatricule;
    
    @FXML
    private TableColumn colDate;
    
    @FXML
    private TableColumn colMedicament;
    
    @FXML
    private TableColumn colQuantite;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void menuSwitch(){
        SceneMenu test = new SceneMenu("Menu.fxml");
    }
    
    public void menuSwitch2(){
        SceneMenu test = new SceneMenu("Panier.fxml");
    }
    
}
