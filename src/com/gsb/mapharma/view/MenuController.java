/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.vo.ProfilVO;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author addev
 */
public class MenuController implements Initializable {

    /**
     * Initializes the controller class.
     */
    @FXML
    private Button btnMedList;
    @FXML
    private Button btnProfil;
    @FXML
    private Button btnMedDossier;
    @FXML
    private Button btnCommande;
    
    private ProfilVO monProfil;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //btnProfil.setOnAction((ActionEvent profil) -> {
         //   sceneProfil();
        //});
    }
    
    
    
    public void sceneProfil() {
//        try {
//            FXMLLoader loader = new FXMLLoader(getClass().getResource("Profil.fxml"));
//            Parent root = loader.load();
//            final ProfilFXController controller = loader.getController();
//            controller.setMonProfil(monProfil);
//            Stage stageProfil = SingletonStage.getStage();
//            Scene sceneP = new Scene(root);
//            stageProfil.setScene(sceneP);
//            stageProfil.show();
//
//        } catch (IOException ex) {
//            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
//        }
        SceneMenu test = new SceneMenu("Profil.fxml");
    }
    
    public void sceneDossierMed() {
        SceneMenu test = new SceneMenu("DossierMed.fxml");
    }
    
    public void sceneCommandes() {
        SceneMenu test = new SceneMenu("Commandes.fxml");
    }
    
    public void sceneCreation() {
        SceneMenu test = new SceneMenu("CreationCompte.fxml");
    }
    
    public void sceneMedicament() {
        SceneMenu test = new SceneMenu("ListeMed.fxml");
    }
    
    public void sceneLogin() {
        SceneMenu test = new SceneMenu("Login.fxml");
    }
    /**
     * @return the idPharma
     */
    public ProfilVO getMonProfil() {
        return monProfil;
    }

    /**
     * @param idPharma the idPharma to set
     */
    public void setMonProfil(ProfilVO idPharma) {
        this.monProfil = idPharma;
    }
}
