/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.vo.MedicamentVO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;


/**
 * FXML Controller class
 *
 * @author rgimenez
 */
public class InfoMedController extends ListeMedController implements Initializable{

    /**
     * Initializes the controller class.
     */
    @FXML private TextField txtCIS;
    @FXML private TextField txtPosologie;    
    
    private MedicamentVO med;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        med = SingletonMed.getInstance().getMed();
        txtCIS.setText(med.getCodeCIS());
        txtPosologie.setText("BONJOUR");
    }
    
    public void listMedSwitch(){
        SceneMenu test = new SceneMenu("ListeMed.fxml");
    }
    
    
}
