/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.vo.ProfilVO;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author addev
 */
public class SceneMenu {

    public SceneMenu(String test) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(test));
            Parent root = loader.load();
            SingletonStage st = SingletonStage.getInstance();
            Stage stageMenu = st.getStage();
            Scene sceneMenu = new Scene(root);
            stageMenu.setScene(sceneMenu);
            stageMenu.show();

        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
