/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.metier.Pharmacien;
import com.gsb.mapharma.vo.ProfilVO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author addev
 */
public class CreationCompteController implements Initializable {

    /**
     * Initializes the controller class.
     */
    
    @FXML private TextField textMatricule;
    
    @FXML private TextField textNom;
            
    @FXML private TextField textPrenom;

    @FXML private TextField textPassword;
    
    private Pharmacien createPharmacien;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void accountSubmit() {
        ProfilVO data = new ProfilVO();
        data.setTxtMatri(textMatricule.getText());
        data.setTxtNom(textNom.getText());
        data.setTxtPrenom(textPrenom.getText());
        data.setPassword(textPassword.getText());
        createPharmacien = new Pharmacien();
        createPharmacien.createUser(data.getTxtMatri(),data.getTxtNom(),data.getTxtPrenom(),data.getPassword());
        
    }
    
    public void resetButton() {
        textMatricule.setText("");
        textNom.setText("");
        textPassword.setText("");
        textPrenom.setText("");
    }
    
    public void menuSwitch(){
        SceneMenu test = new SceneMenu("Menu.fxml");
    }
}
