/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import javafx.stage.Stage;

/**
 *
 * @author addev
 */

public class SingletonStage {
    private Stage pStage;
    private static SingletonStage INSTANCE;
    
    private SingletonStage() {
        
    }
    
    public static SingletonStage getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new SingletonStage();
        }
        return INSTANCE;
    }
    
    public Stage getStage() {
        return this.pStage;
    }
    
    
    public void setStage(Stage stage) {
        this.pStage = stage;
    }
}
