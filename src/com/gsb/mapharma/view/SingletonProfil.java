/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.view;

import com.gsb.mapharma.vo.ProfilVO;

/**
 *
 * @author addev
 */
class SingletonProfil {
    private ProfilVO profil;
    private static SingletonProfil INSTANCE;
    
    private SingletonProfil(){
        profil = new ProfilVO();
    }
    
    public static SingletonProfil getInstance(){
        if (INSTANCE == null) {
            INSTANCE = new SingletonProfil();
        }
        return INSTANCE;
    }
    
    public ProfilVO getProfil(){
        return profil;
    }
    
    public void setProfil(ProfilVO profil){
        this.profil = profil;
    }
    
    
}
