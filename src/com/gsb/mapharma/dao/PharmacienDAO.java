/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gsb.mapharma.dao;

import com.gsb.mapharma.vo.ProfilVO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.scene.Parent;

/**
 *
 * @author addev
 */
public class PharmacienDAO {

    private String login = "GSB";
    private String pass = "";
    private String url = "jdbc:mysql://localhost:3306/gsb_mapharma";

    private Connection cn;
    private Statement st;
    private ResultSet rs;

    public PharmacienDAO() {
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            cn = (Connection) DriverManager.getConnection(url, login, pass);
            st = (Statement) cn.createStatement();
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non reconnu");
        } catch (SQLException ex) {
            System.out.println("Connexion non etablie");
        }

    }

    public ProfilVO getPharmacienByLogin(String matricule, String password) {

        //System.out.println(password.toString());
        ProfilVO monProfil = new ProfilVO();
        try {

            String sql = "select * from Pharmacien where id = " + matricule + ";";
            rs = st.executeQuery(sql);
            Parent menu;
            while (rs.next()) {
                if ((rs.getString("pwd").equals(password))) {
                    monProfil.setTxtMatri(rs.getString("id"));
                    monProfil.setTxtNom(rs.getString("nom"));
                    monProfil.setTxtPrenom(rs.getString("prenom"));
                    monProfil.setTxtRole("PharmacienEnDur");
                    return monProfil;
                } else {
                    System.out.println("Mot de passe ou matricule incorrect");
                }
            }
            cn.close();
            st.close();
            rs.close();
        } catch (SQLException ex) {
            System.out.println("Connexion non etablie");
        }
        return null;
    }
    
    public ProfilVO creationProfil (String matricule,String nom,String prenom,String password) {
        try {
            String sql = "INSERT INTO Pharmacien('id','nom','prenom','pwd') VALUES('"+ matricule
                        + "','"+ nom +"','"+ prenom +"','"+ password +"');";
            System.out.println(sql);
            st.executeUpdate(sql);
            cn.close();
            st.close();
            rs.close();
        } catch (SQLException ex) {
            
            System.out.println("Connexion non etablie");
        }
        return null;
    }
}
