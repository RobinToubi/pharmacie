/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gsb.mapharma.dao;

import com.gsb.mapharma.vo.MedicamentVO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author addev
 */
public class MedicamentDAO {

    private String url = "jdbc:mysql://localhost:3306/gsb_mapharma";
    private String login = "GSB";
    private String pass = "";
    private String sql = "select * from medList;";

    private Connection cn;
    private Statement st;
    private ResultSet rs;

    public MedicamentDAO() {
        
    }
        
    /**
     *
     * @return medicaments
     */
    
    public ArrayList<MedicamentVO> readMedicamentByArrayList() {
        ArrayList<MedicamentVO> medicaments = new ArrayList<>();
        try {
            Class.forName("org.mariadb.jdbc.Driver");
            cn = (Connection) DriverManager.getConnection(url, login, pass);
            st = (Statement) cn.createStatement();
            rs = st.executeQuery(sql);
            while (rs.next()) {
                MedicamentVO medicament = new MedicamentVO();
                medicament.setColNomMed(rs.getString("NomMed"));
                medicament.setColFormeMed(rs.getString("FormeMed"));
                medicament.setColCodeCIS(rs.getString("CodeCIS"));
                medicaments.add(medicament);
            }
            cn.close();
            st.close();
            rs.close();
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non reconnu");
        } catch (SQLException ex) {
            System.out.println("Connexion non etablie");
        }
        return medicaments;
    }
}