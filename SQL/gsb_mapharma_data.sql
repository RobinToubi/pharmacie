-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- Dump data of "Client" -----------------------------------
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '1', '', '', '699764444' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '2', '', '', '693405983' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '3', '', '', '680082661' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '4', '', '', '655370606' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '5', '', '', '692923996' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '6', '', '', '640535004' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '7', '', '', '648464102' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '8', '', '', '626706007' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '9', '', '', '604288220' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '10', '', '', '623614499' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '11', '', '', '675148120' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '12', '', '', '636171737' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '13', '', '', '673579745' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '14', '', '', '666184001' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '15', '', '', '606181481' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '16', '', '', '606611136' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '17', '', '', '653155019' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '18', '', '', '691747617' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '19', '', '', '691315983' );
INSERT INTO `Client`(`idCli`,`NomCli`,`PrenomCli`,`TelCli`) VALUES ( '20', '', '', '654873433' );
-- ---------------------------------------------------------


-- Dump data of "Pharmacien" -------------------------------
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '1', 'Clinton', 'Dwight', 'root123' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '2', 'Carter', 'Richard', '6Z19KWay' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '3', 'Arthur', 'Thomas', 'fdMEggzc' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '4', 'Polk', 'Harry', 'ViT9H7zb' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '5', 'Monroe', 'George', '1JMyRS4f' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '6', 'Quincy', 'Rutherford', 'uxonXFKc' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '7', 'Kennedy', 'Bill', 'oArQNfOi' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '8', 'Harrison', 'Theodore', 'yZq3H6Db' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '9', 'Cleveland', 'Harry', 'ndtjS3eJ' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '10', 'Madison', 'Benjamin', 'C1a5xIj3' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '11', 'Van Buren', 'Herbert', 'rYmZOMIo' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '12', 'Nixon', 'Warren', '9w8jIiNh' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '13', 'Eisenhower', 'Franklin', 'I6iNCn2Z' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '14', 'Reagan', 'Chester', 'rjODQ67K' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '15', 'Hayes', 'Zachary', 'QlHd8l7c' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '16', 'Taylor', 'William', 'sRQnTIPp' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '17', 'McKinley', 'Theodore', '9YtPp8bV' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '18', 'Johnson', 'Calvin', 'I415l8Ud' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '19', 'Pierce', 'Ulysses', 'ePVu8s0T' );
INSERT INTO `Pharmacien`(`id`,`nom`,`prenom`,`pwd`) VALUES ( '20', 'Polk', 'Chester', 'ALdhJGoo' );
-- ---------------------------------------------------------


-- Dump data of "medList" ----------------------------------
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60282999', 'ABSTRAL 400 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60538772', 'ABIES CANADENSIS BOIRON, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60599533', 'ABILIFY 5 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60612094', 'ABSINTHIUM LEHNING, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60667087', 'ABILIFY MAINTENA 400 mg, poudre et solvant pour suspension injectable a� liberation prolongee', 'poudre et  solvant pour suspension injectable a� liberation prolongee' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '60946941', 'ACARBOSE MYLAN 100 mg, ', '' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '61266250', 'A 313 200 000 UI POUR CENT, pommade', 'pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '61464150', 'ABSTRAL 300 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '61578296', 'ABSTRAL 800 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '61815891', 'ABILIFY 10 mg, comprime orodispersible', 'comprime orodispersible' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '61937888', 'ACARBOSE EG 50 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62024766', 'ABROTANUM WELEDA, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', 'granules et  cra�me et  solution en gouttes en gouttes' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62082709', 'ABROTANUM BOIRON, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62170486', 'ABACAVIR/LAMIVUDINE MYLAN PHARMA 600 mg/300 mg, comprime pellicule', 'comprime pellicule' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62425595', 'ABIES PECTINATA WELEDA, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', 'granules et  solution en gouttes en gouttes' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62869109', 'A 313 50 000 U.I., capsule molle', 'capsule molle' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '62963238', 'ACALYPHA INDICA BOIRON, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '63075735', 'ABIES PECTINATA BOIRON, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '63185053', 'ACADIONE 250 mg, comprime drageifie', 'comprime drageifie' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '63431640', 'ABACAVIR/LAMIVUDINE BIOGARAN 600 mg/300 mg, comprime pellicule', 'comprime pellicule' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '63451714', 'ABSTRAL 600 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '63958735', 'ACARBOSE ARROW LAB 50 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64055988', 'ABILIFY 1 mg/ml, solution buvable', 'solution buvable' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64127672', 'ACARBOSE BIOGARAN 100 mg, comprime secable', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64224801', 'ABILIFY 15 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64332894', 'ABASAGLAR 100 unites/ml, solution injectable en stylo prerempli', 'solution injectable' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64457435', 'ABSINTHIUM BOIRON, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '64728712', 'ABUFENE 400 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '65198099', 'ABILIFY 10 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '65555163', 'ACARBOSE ARROW 100 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '65698396', 'ABIES NIGRA BOIRON, degre de dilution compris entre 2CH a� 30CH et 4DH a� 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '65803331', 'ACARBOSE EG 100 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '65857902', 'ABACAVIR/LAMIVUDINE TEVA 600 mg/300 mg, comprime pellicule', 'comprime pellicule' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '66207341', 'ABELCET 5 mg/ml, suspension a� diluer pour perfusion', 'suspension a� diluer pour perfusion' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '66513085', 'ABASAGLAR 100 unites/ml, solution injectable en cartouche', 'solution injectable' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '66706942', 'ABILIFY MAINTENA 400 mg, poudre et solvant pour suspension injectable a� liberation prolongee en seringue preremplie', 'poudre et  solvant pour suspension injectable a� liberation prolongee' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '66791654', 'ABILIFY 15 mg, comprime orodispersible', 'comprime orodispersible' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '67144513', 'ABSTRAL 200 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '67720261', 'ABACAVIR/LAMIVUDINE SANDOZ 600 mg/300 mg, comprime pellicule', 'comprime pellicule' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '67871676', 'ABROTANUM LEHNING, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '68336954', 'ABSTRAL 100 microgrammes, comprime sublingual', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '68371745', 'ACARBOSE ARROW LAB 100 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '68729480', 'ABILIFY MAINTENA 300 mg, poudre et solvant pour suspension injectable a� liberation prolongee', 'poudre et  solvant pour suspension injectable a� liberation prolongee' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '68739019', 'ABRAXANE 5 mg/ml, poudre pour suspension injectable pour perfusion', 'poudre pour suspension injectable pour perfusion' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '69116854', 'ACARBOSE BIOGARAN 50 mg, comprime', 'comprime' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '69641873', 'ABILIFY 7,5 mg/ml, solution injectable', 'solution injectable' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '69724187', 'ACAMPROSATE BIOGARAN 333 mg, comprime pellicule gastro-resistant', 'comprime pellicule gastro-resistant(e)' );
INSERT INTO `medList`(`CodeCIS`,`NomMed`,`FormeMed`) VALUES ( '69985792', 'ABIES PECTINATA LEHNING, degre de dilution compris entre 2CH et 30CH ou entre 4DH et 60DH', ' comprime et solution(s) et granules et poudre et pommade' );
-- ---------------------------------------------------------


-- Dump data of "medPresentation" --------------------------
INSERT INTO `medPresentation`(`CodeCIS`,`CodeCIP7`,`TauxRemboursement`,`Prix`,`VoiesAdministration`) VALUES ( '60002283', '4949729', '100%', '43,69', NULL );
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


