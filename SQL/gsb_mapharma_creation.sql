-- Valentina Studio --
-- MySQL dump --
-- ---------------------------------------------------------


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
-- ---------------------------------------------------------


-- CREATE DATABASE "gsb_mapharma" --------------------------
CREATE DATABASE IF NOT EXISTS `gsb_mapharma` CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `gsb_mapharma`;
-- ---------------------------------------------------------


-- CREATE TABLE "Client" -----------------------------------
-- CREATE TABLE "Client" ---------------------------------------
CREATE TABLE `Client` ( 
	`idCli` Int( 11 ) NULL,
	`NomCli` VarChar( 0 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`PrenomCli` VarChar( 0 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`TelCli` Int( 11 ) NULL )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "Pharmacien" -------------------------------
-- CREATE TABLE "Pharmacien" -----------------------------------
CREATE TABLE `Pharmacien` ( 
	`id` Int( 11 ) NULL,
	`nom` VarChar( 80 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`prenom` VarChar( 80 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`pwd` VarChar( 12 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "medList" ----------------------------------
-- CREATE TABLE "medList" --------------------------------------
CREATE TABLE `medList` ( 
	`CodeCIS` Int( 8 ) NOT NULL DEFAULT '0',
	`NomMed` VarChar( 115 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`FormeMed` VarChar( 68 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `CodeCIS` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


-- CREATE TABLE "medPresentation" --------------------------
-- CREATE TABLE "medPresentation" ------------------------------
CREATE TABLE `medPresentation` ( 
	`CodeCIS` Int( 8 ) NOT NULL DEFAULT '0',
	`CodeCIP7` Int( 7 ) NULL,
	`TauxRemboursement` VarChar( 4 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`Prix` VarChar( 6 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	`VoiesAdministration` VarChar( 25 ) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
	PRIMARY KEY ( `CodeCIS` ) )
CHARACTER SET = latin1
COLLATE = latin1_swedish_ci
ENGINE = InnoDB;
-- -------------------------------------------------------------
-- ---------------------------------------------------------


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
-- ---------------------------------------------------------


