#Mise en place d'un progiciel

En effet, l'entreprise GSB (entreprise fictive dans le cadre de notre cursus éducatif) nous a proposé de créer leur logiciel de gestion de stock de leurs médicaments.
Pour cela, nous avons créé un groupe de 6 personnes pour effectuer ce travail.
Les technologies utilisées dans ce projet sont les suivantes : Java SE, SQL, SceneBuilder (JavaFX).

### Fonctionnalitées

Sur notre progiciel, nous pouvons tout d'abord nous connecter (obviously) et il y a un système de connexion relié à une base de données.
Ensuite, si notre connexion est effectuée, nous arrivons sur le panel principal, montrant les différentes opérations possibles.
Voici ci-dessous les opérations que nous pouvons effectuer :
 - Ajouter un utilisateur dans la base de donnnées
 - Afficher une liste complète de médicaments + avoir toutes les spécificités en cliquant dessus
 - Voir le dossier médical des patients 
 - Voir son profil ainsi que ses coordonnées

Le projet fut arrêté par manque de temps et je ne pense pas qu'il soit remis en ordre par moi-même (RobinToubi, créateur du projet).


